# conlog
_a constructive logic proof checker_

## Inspiration

After taking [CMU SCS' 15-317](https://csd.cmu.edu/course-profiles/15-317-Constructive-Logic) in the fall of 2019, I became very interested in constructive (intuitionistic) logic. So, this project is a learning project to "re-develop" the [tutch](http://www2.tcs.ifi.lmu.de/~abel/tutch/) language. In this project, you'll see a lot of learning involved of how [Standard ML](https://www.smlnj.org/) is used to build a proof checker.

As of February 13, 2021, there is no clear development plan, as right now I am in the learning phase of this project.

## Task List

- [ ] Learn how SML is used to parse and tokenize an input
- [ ] Learn how to cleanly define multiple proof checking rules in SML
- [ ] Determine what proof checking rules will be implemented
- [ ] Determine what language this will be implemented in
- [ ] Set up test suite
- [ ] Set up CI/CD within GitLab

## License

This project is licensed under the GNU Affero General Public License v3.0. To view the license, see [LICENSE](LICENSE). To learn more about the license, read here: [https://choosealicense.com/licenses/agpl-3.0/](https://choosealicense.com/licenses/agpl-3.0/).
